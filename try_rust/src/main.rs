

extern crate csv;
extern crate rustc_serialize;
extern crate docopt;

use docopt::Docopt;
use csv::{Reader, Writer};
mod server;
use server::Server;

static USAGE: &'static str = "
Usage: try <port> [options] [<file>]

Options:
    -h, --help          show this screen
    -i, --input-file    name of input file
    -o, --output-file   name of input file
    -v, --version       output version information and exit
";

#[derive(RustcDecodable)]
struct Args {
    arg_port: String,
    arg_file: Option<String>,
    flag_help: bool,
    flag_input_file: bool,
    flag_output_file: bool,
}

fn main() {
    let args: Args = Docopt::new(USAGE).and_then(|d| d.decode()).unwrap_or_else(|e| e.exit());
    if (args.flag_input_file) {
        println!("Inputfile flag");
    }
    if (args.flag_output_file) {
        println!("Outputfile flag");
    }
    match args.arg_file {
        // The division was valid
        Some(x) => println!("Name of file: {}", x),
        // The division was invalid
        None => println!("No file set"),
    }


    let dollar_films = vec![
    ("A Fistful of Dollars", "Rojo", 1964u32),
    ("For a Few Dollars More", "El Indio", 1965u32),
    ("The Good, the Bad and the Ugly", "Tuco", 1966u32),
    ];
    let path = "westerns.csv";
    let mut writer = Writer::from_file(&path).unwrap();
    for row in dollar_films.into_iter() {
        writer.encode(row).expect("Erreur");
    }
    let port: i32 = args.arg_port.parse::<i32>().unwrap();
    let server = Server::new(port);
    server.start();
}
