extern crate std;
extern crate iron;
extern crate router;
extern crate rustc_serialize;

use self::iron::prelude::*;
use self::iron::status;
use self::router::Router;
use self::rustc_serialize::{json,Decodable};
use std::fmt::{self, Formatter, Display};
use std::io::Read;

#[derive(RustcEncodable,RustcDecodable)]
struct Greeting {
    msg: String
}

fn hello_world(_: &mut Request) -> IronResult<Response> {
    let greeting = Greeting { msg: "Salut, World".to_string() };
    let payload = json::encode(&greeting).unwrap();
    Ok(Response::with((status::Ok, payload)))
}

// Receive a message by POST and play it back.
fn set_greeting(request: &mut Request) -> IronResult<Response> {
    let mut payload = String::new();
    request.body.read_to_string(&mut payload).unwrap();
    let request: Greeting = json::decode(&payload).unwrap();
    let greeting = Greeting { msg: request.msg };
    let payload = json::encode(&greeting).unwrap();
    Ok(Response::with((status::Ok, payload)))
}


pub struct Server {
    port: i32,
    router: Router
}

impl Server {
    pub fn new(x:i32) -> Server {
        let mut server = Server{
            port: x,
            router : Router::new()
        };
        server.router.get("/", hello_world);
        server.router.post("/set", set_greeting);
        return server
    }

    pub fn start(self) {
        let f = format!("localhost:{}", self.port);
        println!("Launching server on port {}", self.port);
        Iron::new(self.router).http(f.as_str()).unwrap();
        println!("Server started {}", self.port);
    }
}
